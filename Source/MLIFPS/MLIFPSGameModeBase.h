// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MLIFPSGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MLIFPS_API AMLIFPSGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
