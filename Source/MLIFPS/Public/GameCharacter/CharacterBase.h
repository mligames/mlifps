// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "CharacterBase.generated.h"

class UCameraComponent;
class USceneComponent;
class USpringArmComponent;
class UHealthComponent;
class AWeaponBase;

UCLASS(ABSTRACT)
class MLIFPS_API ACharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ACharacterBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

/* Movement */
	void MoveForward(float Value);

	void MoveRight(float Value);

	void BeginCrouch();

	void EndCrouch();

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	//USceneComponent* RightHandRoot;
/* Camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	USpringArmComponent* SpringArmComp;

/* Heath */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	UHealthComponent* HealthComp;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Player")
	bool bDied;

	UFUNCTION()
	void OnHealthChanged(UHealthComponent* OwningHealthComp, float Health, float HealthDelta, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

/* Weapon */
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Player")
	FName RightHandSocketName;

	UPROPERTY(Replicated)
	AWeaponBase* CurrentWeapon;

	UPROPERTY(EditDefaultsOnly, Category = "Player")
	TSubclassOf<AWeaponBase> StarterWeaponClass;

/** Aim Offset Networking */
	/** get aim offsets */
	UFUNCTION(BlueprintCallable, Category = "Player")
	FRotator GetAimOffsets() const;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual FVector GetPawnViewLocation() const override;

	UFUNCTION(BlueprintCallable, Category = "Player")
	void StartFire();

	UFUNCTION(BlueprintCallable, Category = "Player")
	void StopFire();

/* ADS State Networking */
protected:

	/** current targeting state */
	UPROPERTY(Transient, Replicated)
	bool bIsADS;

	/** [server + local] change targeting state */
	void SetADS(bool bNewADS);

	/** player pressed targeting action */
	void OnStartADS();

	/** player released targeting action */
	void OnStopADS();

	/** update targeting state */
	UFUNCTION(server, reliable, WithValidation)
	void ServerSetADS(bool bNewADS);

	UPROPERTY(EditDefaultsOnly, Category = "Player", meta = (ClampMin = 0.1, ClampMax = 100))
	float ADSInterpSpeed;

public:
	UPROPERTY(EditDefaultsOnly, Category = "Player")
	float ADSSpeedModifier;
	/** get weapon taget modifier speed	*/
	UFUNCTION(BlueprintCallable, Category = "Player")
	float GetADSSpeedModifier() const;

	UFUNCTION(BlueprintCallable, Category = "Player")
	bool IsADS() const;

};
