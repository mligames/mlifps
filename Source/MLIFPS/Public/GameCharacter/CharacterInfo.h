// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "CharacterInfo.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct MLIFPS_API FCharacterInfo
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = MyCategory)
	FString CharacterName;
};
