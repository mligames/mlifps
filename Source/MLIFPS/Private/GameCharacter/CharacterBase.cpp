// Fill out your copyright notice in the Description page of Project Settings.


#include "GameCharacter/CharacterBase.h"
#include "MLIFPS/MLIFPS.h"
#include "Camera/CameraComponent.h"
#include "Components/CharacterBaseMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/HealthComponent.h"
#include "Weapon/WeaponBase.h"
#include "Net/UnrealNetwork.h"

//ACharacterBase::ACharacterBase(const class FObjectInitializer& ObjectInitializer): ACharacter(ObjectInitializer.SetDefaultSubobjectClass<UCharacterBaseMovementComponent>(ACharacter::CharacterMovementComponentName))
// Sets default values
ACharacterBase::ACharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
	SpringArmComp->bUsePawnControlRotation = true;
	SpringArmComp->SetupAttachment(RootComponent);

	GetMovementComponent()->GetNavAgentPropertiesRef().bCanCrouch = true;

	GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetupAttachment(SpringArmComp);

	HealthComp = CreateDefaultSubobject<UHealthComponent>(TEXT("HealthComp"));

	//SetReplicates(true);
	//bReplicates = true;

}

// Called when the game starts or when spawned
void ACharacterBase::BeginPlay()
{
	Super::BeginPlay();

	UE_LOG(LogTemp, Warning, TEXT("CharacterBase BeginPlay"));
	if (HasAuthority())
	{
		// Spawn a default weapon
		FActorSpawnParameters SpawnParams;
		SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		CurrentWeapon = GetWorld()->SpawnActor<AWeaponBase>(StarterWeaponClass, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
		if (CurrentWeapon)
		{
			CurrentWeapon->SetOwner(this);
			CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, RightHandSocketName);
		}
	}
}

void ACharacterBase::MoveForward(float Value)
{
	AddMovementInput(GetActorForwardVector() * Value);
}

void ACharacterBase::MoveRight(float Value)
{
	AddMovementInput(GetActorRightVector() * Value);
}

void ACharacterBase::BeginCrouch()
{
	Crouch();
}

void ACharacterBase::EndCrouch()
{
	UnCrouch();
}

void ACharacterBase::OnHealthChanged(UHealthComponent* OwningHealthComp, float Health, float HealthDelta, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	GEngine->AddOnScreenDebugMessage(0, 1.0f, FColor::Red, FString::Printf(TEXT("OnHealthChanged Called")));

	//UE_LOG(LogTemp, Display, TEXT("Hit %s. Health Delta %f"), GetName(), HealthDelta);

	if (Health <= 0.0f && !bDied)
	{
		// Die!
		bDied = true;

		GetMovementComponent()->StopMovementImmediately();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

		DetachFromControllerPendingDestroy();

		SetLifeSpan(10.0f);
	}
}

FRotator ACharacterBase::GetAimOffsets() const
{
	const FVector AimDirWorldSpace = GetBaseAimRotation().Vector();
	const FVector AimDirLocalSpace = ActorToWorld().InverseTransformVectorNoScale(AimDirWorldSpace);
	const FRotator AimRotLocalSpace = AimDirLocalSpace.Rotation();

	return AimRotLocalSpace;
}

// Called every frame
void ACharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ACharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &ACharacterBase::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACharacterBase::MoveRight);

	PlayerInputComponent->BindAxis("LookUp", this, &ACharacterBase::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookRight", this, &ACharacterBase::AddControllerYawInput);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ACharacterBase::BeginCrouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ACharacterBase::EndCrouch);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ACharacterBase::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ACharacterBase::StopFire);

	PlayerInputComponent->BindAction("AimDownSight", IE_Pressed, this, &ACharacterBase::OnStartADS);
	PlayerInputComponent->BindAction("AimDownSight", IE_Released, this, &ACharacterBase::OnStopADS);
}

FVector ACharacterBase::GetPawnViewLocation() const
{
	if (CameraComp)
	{
		return CameraComp->GetComponentLocation();
	}

	return Super::GetPawnViewLocation();
}


// Networked ADS
void ACharacterBase::SetADS(bool bNewADS)
{
	bIsADS = bNewADS;


	if (GetLocalRole() < ROLE_Authority)
	{
		ServerSetADS(bNewADS);
	}
}

bool ACharacterBase::ServerSetADS_Validate(bool bNewADS)
{
	return true;
}

void ACharacterBase::ServerSetADS_Implementation(bool bNewADS)
{
	SetADS(bNewADS);
}

void ACharacterBase::OnStartADS()
{
	SetADS(true);
}

void ACharacterBase::OnStopADS()
{
	SetADS(false);
}

float ACharacterBase::GetADSSpeedModifier() const
{
	return ADSSpeedModifier;
}

bool ACharacterBase::IsADS() const
{
	return bIsADS;
}


void ACharacterBase::StartFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StartFire();
	}
}

void ACharacterBase::StopFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
	}
}
void ACharacterBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ACharacterBase, CurrentWeapon);
	DOREPLIFETIME(ACharacterBase, bDied);
	DOREPLIFETIME_CONDITION(ACharacterBase, bIsADS, COND_SkipOwner);
}

