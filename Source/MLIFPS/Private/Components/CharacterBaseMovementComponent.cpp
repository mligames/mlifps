// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/CharacterBaseMovementComponent.h"
#include "GameCharacter/CharacterBase.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"

UCharacterBaseMovementComponent::UCharacterBaseMovementComponent() 
{
	SetIsReplicated(true);
}

float UCharacterBaseMovementComponent::GetMaxSpeed() const
{
	float MaxSpeed = Super::GetMaxSpeed();

	const ACharacterBase* CharacterBaseOwner = Cast<ACharacterBase>(PawnOwner);
	if (CharacterBaseOwner)
	{
		if (CharacterBaseOwner->IsADS())
		{
			MaxSpeed *= CharacterBaseOwner->GetADSSpeedModifier();
		}
	}

	return MaxSpeed;
}

